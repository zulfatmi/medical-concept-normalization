from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse.linalg import norm as sparse_norm
from nltk.tokenize import wordpunct_tokenize
from nltk import wordpunct_tokenize
from sklearn.cluster import KMeans
from sentence_similarity import *
from numpy.linalg import norm
from pandas import read_csv
import cPickle as pickle
from tqdm import tqdm
import numpy as np
import codecs
import os


TFIDF = 'data/tfidf.bin'
CLST_CENTERS = 'data/clusters'


def wp_tokenizer(x): return wordpunct_tokenize(x)


def save(obj, path):
    with codecs.open(path, 'wb') as output_file:
        pickle.dump(obj, output_file)


def load(path):
    with codecs.open(path, 'rb') as input_file:
        return pickle.load(input_file)


def vectorize_concepts(df):
    if not os.path.isfile(TFIDF):
        tfidf_vectorizer = TfidfVectorizer(tokenizer=wp_tokenizer, max_features=10000)
        tfidf_vectorizer.fit(df.phrase.values)
        save(tfidf_vectorizer,TFIDF)
    else:
        tfidf_vectorizer = load(TFIDF)
    return tfidf_vectorizer.transform(df.phrase.values)


def get_distances(phrases):
    tfidf_vectorizer = load(TFIDF)
    vectorized_phrases = tfidf_vectorizer.transform(phrases)
    cluster_centers = load(CLST_CENTERS)
    dproducts = vectorized_phrases.dot(cluster_centers.T)
    phrase_norms = sparse_norm(vectorized_phrases, axis=1)
    cluster_norms = norm(cluster_centers, axis=1)
    phrase_norms[phrase_norms == 0] = 1.0
    cluster_norms[cluster_norms == 0] = 1.0
    return dproducts/np.expand_dims(phrase_norms, axis=1)/np.expand_dims(cluster_norms, axis=0)


def load_concepts():
    with codecs.open('data/concepts.txt') as input_file:
        return input_file.readlines()


def get_distances_on_concepts(phrases):
    tfidf_vectorizer = load(TFIDF)
    vectorized_phrases = tfidf_vectorizer.transform(phrases)
    concepts = load_concepts()
    vectorized_concepts = tfidf_vectorizer.transform(concepts)
    dproducts = vectorized_phrases.dot(vectorized_concepts.T)
    phrase_norms = sparse_norm(vectorized_phrases, axis=1)
    cluster_norms = norm(vectorized_concepts, axis=1)
    phrase_norms[phrase_norms == 0] = 1.0
    vectorized_concepts[vectorized_concepts == 0] = 1.0
    return dproducts/np.expand_dims(phrase_norms, axis=1)/np.expand_dims(cluster_norms, axis=0)


def get_distances_to_nearest_concepts(phrases):
    tfidf_vectorizer = load(TFIDF)
    phrases = [unicode(phrase, errors='ignore') for phrase in phrases]
    vectorized_phrases = tfidf_vectorizer.transform(phrases)
    concepts = read_csv('data/concepts_raw.csv')
    cuis = concepts.cui.unique()
    vectorized_concepts = tfidf_vectorizer.transform(concepts.phrase)
    dproducts = vectorized_phrases.dot(vectorized_concepts.T)
    phrase_norms = sparse_norm(vectorized_phrases, axis=1)
    cluster_norms = sparse_norm(vectorized_concepts, axis=1)
    phrase_norms[phrase_norms == 0.0] = 1.0
    cluster_norms[cluster_norms == 0.0] = 1.0
    sims = dproducts/np.expand_dims(phrase_norms, axis=1)/np.expand_dims(cluster_norms, axis=0)
    cosine_sims = np.zeros([len(phrases), len(cuis)])

    for i, cui in enumerate(cuis):                                          #
        cui_index = concepts[concepts.cui==cui].index                       #
        cosine_sims[:, i] = sims[:, cui_index].max(axis=1).reshape([len(phrases)])     #
    return cosine_sims


def get_distances_w2v(phrases, w2v_model):

    concepts = load_concepts()
    concepts_cnt = len(concepts)
    phrases_cnt = len(phrases)
    tokenized_concepts = map(wordpunct_tokenize, concepts)
    tokenized_phrases = map(wordpunct_tokenize, phrases)
    vectorized_concepts = np.zeros([concepts_cnt, w2v_model.vector_size])
    for i, tokenized_concept in enumerate(tokenized_concepts):
        for token in tokenized_concept:
            if token in w2v_model: vectorized_concepts[i] += w2v_model[token]

    vectorized_phrases = np.zeros([phrases_cnt, w2v_model.vector_size])
    for i, tokenized_phrase in enumerate(tokenized_phrases):
        for token in tokenized_phrase:
                if token in w2v_model: vectorized_phrases[i] += w2v_model[token]

    dproducts = vectorized_phrases.dot(vectorized_concepts.T)
    phrase_norms = norm(vectorized_phrases, axis=1)
    cluster_norms = norm(vectorized_concepts, axis=1)
    phrase_norms[phrase_norms == 0.0] = 1.0
    cluster_norms[cluster_norms == 0.0] = 1.0
    return dproducts/np.expand_dims(phrase_norms, axis=1)/np.expand_dims(cluster_norms, axis=0)


def get_distances_to_nearest_concepts_d(phrases):

    concepts = read_csv('data/concepts_raw.csv')
    concepts['phrase'] = concepts.phrase.apply(wordpunct_tokenize)

    unique_concepts_count = concepts.cui.unique().shape[0]
    phrases_count = len(phrases)
    similarities = np.zeros([phrases_count, unique_concepts_count])

    for i, (cui, group) in enumerate(concepts.groupby('cui')):
        similarities_column = []
        for phrase in phrases:
            sim_f = lambda s: jaccard_sim(phrase, s)
            sims = map(sim_f, group['phrase'])
            similarities_column.append(max(sims))
        similarities_column = np.array(similarities_column)
        similarities[:,i] = similarities_column

    return similarities


def prepare_clusters():
    df = read_csv('data/umls_dataset.csv', sep='\t', names=['phrase', 'cuid'])
    df = df.groupby('cuid').agg(' '.join)
    print "Total concepts count {:d}".format(df.shape[0])
    vectorized_concepts = vectorize_concepts(df)
    print "Vectorized concepts"
    kmeans = KMeans(n_clusters=5000, random_state=42, n_jobs=-1)
    kmeans.fit(vectorized_concepts)
    cluster_centers = kmeans.cluster_centers_
    save(cluster_centers, CLST_CENTERS)

if __name__ == '__main__':
    print get_distances_to_nearest_concepts(['phrase number one', 'phrase number two'])
