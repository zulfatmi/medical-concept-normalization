from nltk.tokenize import wordpunct_tokenize
import numpy as np
import pickle
import codecs
import json
import nltk

pos_tagger = nltk.data.load('taggers/maxent_treebank_pos_tagger/english.pickle')

def read_flat_data(file_path, spell_checker=None):
    with codecs.open(file_path) as in_file:
        data = [line.lower().strip('\n').split('\t') for line in in_file]

    utterances = np.array([wordpunct_tokenize(line[0]) for line in data])
    if spell_checker is not None: utterances = np.array([map(spell_checker.correct, utt) for utt in utterances])
    concepts = np.array([line[1] for line in data])

    return utterances, None, concepts



def read_data(file_path):
    with codecs.open(file_path, 'rb') as in_file:
        return pickle.load(in_file)

def create_token_index(utterances, pd_val='PADDING'):
    index = {pd_val:0}
    tokens = [pd_val]
    for utterance in utterances:
        for token in utterance:
            if token not in index:
                index[token] = len(index)
                tokens.append(token)

    index['UNKNOWN'] = len(index)
    tokens.append('UNKNOWN')
    return index, tokens

def create_concept_index(concepts):
    concept_index = {}
    concepts_list = []
    for concept in concepts:
        if concept not in concept_index:
            concept_index[concept] = len(concept_index)
            concepts_list.append(concept)
    concept_index['UNKNOWN'] = len(concept_index)
    concepts_list.append('UNKNOWN')
    return concept_index, concepts_list

def tokens2ids(utterances, token_index):
    return [[token_index[token] if token in token_index else token_index['UNKNOWN'] for token in utterance] for utterance in utterances]

def concepts2ids(concepts, concept_index):
    return [concept_index[concept] if concept in concept_index else concept_index['UNKNOWN'] for concept in concepts]

def pad(x, length, padding_value=0):
    if len(x) < length: return x + [padding_value]*(length-len(x))
    else: return x[:length]

def get_embeddings(token_index, w2v_model, embeddings_dim=300):
    vocabulary_size = len(token_index)
    scale = 1.0/(np.sqrt(vocabulary_size))
    scale = 1.0
    embeddings = scale*np.random.randn(vocabulary_size, embeddings_dim)
    for index, token in token_index.iteritems():
        if token in w2v_model: embeddings[index] = scale*w2v_model[token]

    return embeddings.astype(np.float32)

def load_json_data(path):
    with codecs.open(path) as input_file:
        reviews = map(json.loads, input_file.readlines())
    utterances, dates, hookups = [], [], []
    for i, review in enumerate(reviews):
        if 'entities_pred' not in review: continue
        review_entities = review['entities_pred']
        for entity_key, entity in review_entities.iteritems():
                hookup = (review_entities, entity_key)
                utterance = wordpunct_tokenize(entity['text'])
                utterances.append(utterance)
                dates.append(review['reviewdate'])
                hookups.append(hookup)

    return reviews, utterances, dates, hookups

def save_json(reviews, predicted_concepts, hookups):

    for hookup, predicted_concept in zip(hookups, predicted_concepts):
        review_entities, entity_key = hookup
        review_entities[entity_key]['concept'] = predicted_concept

    with codecs.open('dcom_normalized.txt', 'w') as out_file:
        dumped_reviews = map(json.dumps, reviews)
        out_file.write('\n'.join(dumped_reviews))


def save_index(path, index, names):
    with codecs.open(path, 'wb') as output_file:
        pickle.dump((index, names), output_file)

def restore_index(path):
    with codecs.open(path, 'rb') as input_file:
        return pickle.load(input_file)


def batch_indexes(bsize, dataset_size):
    batches_cnt = (dataset_size + bsize - 1) / bsize
    indexes = np.random.permutation(dataset_size)
    for batch_snum in xrange(batches_cnt):
        start_from = batch_snum*bsize
        up_to = min(dataset_size, (batch_snum+1)*bsize)
        yield indexes[start_from:up_to]


def get_pos_tags(utterances):
    tagged_utterances = map(pos_tagger.tag, utterances)
    return [[pos_tag for token, pos_tag in tagged_utterance] for tagged_utterance in tagged_utterances]

