from pandas import read_csv
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--resfile', dest='results_file')
    parser.add_argument('--epochs', dest='epochs', type=int)
    args = parser.parse_args()
    df = read_csv(args.results_file, names=['epoch_{}'.format(i) for i in range(args.epochs)], sep='\t')
    cv_scores = df.apply(np.mean).values
    print 'CV max score:', max(cv_scores)
    print 'On', np.argmax(cv_scores)
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    for index, row in df.iterrows():
        plt.plot(row.values)
    plt.savefig(args.results_file.replace('csv', 'png'), dpi=300)
    plt.close()
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.plot(cv_scores)
    plt.savefig('mean' + args.results_file.replace('csv', 'png'), dpi=300)

