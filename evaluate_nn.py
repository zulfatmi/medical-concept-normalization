from train import train
import os
from argparse import ArgumentParser
from gensim.models import KeyedVectors
import codecs
from knowledge import wp_tokenizer
import tensorflow as tf

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--foldsfolder', dest='folds_folder', default='data/folds/context_0/')
    parser.add_argument('--word2vec', dest='w2v_model_path', default='')
    parser.add_argument('--epochs', dest='epochs', type=int, default=50)
    parser.add_argument('--batch_size', dest='batch_size', type=int, default=32)
    parser.add_argument('--learning_rate', dest='learning_rate', type=float, default=1e-3)
    parser.add_argument('--hidden_units', dest='hidden_units', type=int, default=100)
    parser.add_argument('--device', dest='device', default='/cpu:0')
    parser.add_argument('--context', dest='context_size', default=0)
    parser.add_argument('--flat', dest='flat', action="store_true")
    args = parser.parse_args()
    all_files = os.listdir(args.folds_folder)
    train_files = sorted([file_name for file_name in all_files if 'train' in file_name])
    test_files = sorted([file_name for file_name in all_files if 'test' in file_name])
    w2v_model = {} #KeyedVectors.load_word2vec_format(args.w2v_model_path, binary=True)

    accuracies = []
    for train_file, test_file in zip(train_files, test_files):
        train_file_path = os.path.join(args.folds_folder, train_file)
        test_file_path = os.path.join(args.folds_folder, test_file)
        with tf.variable_scope(train_file), tf.device(args.device):
            max_accuracy = train(w2v_model, train_file_path, test_file_path, args.batch_size, args.epochs, args.learning_rate, args.hidden_units, args.flat, args.context_size)

        accuracies.append(max_accuracy)

    log_file = 'accuracies_{}_{}_{}_{}_biocr.csv'.format(args.batch_size, int(args.learning_rate*1e5), args.hidden_units, os.path.basename(args.w2v_model_path))

    with codecs.open(log_file, 'w', encoding='utf-8') as out_file:
        accuracies = map(str, accuracies)
        out_file.write('\n'.join(accuracies))
