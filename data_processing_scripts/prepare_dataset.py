import json
import codecs
from nltk.tokenize import wordpunct_tokenize
import re


def extract_left_context(text, cnt):
    tokenized_texts = wordpunct_tokenize(text)
    cnt = len(tokenized_texts) - cnt
    return ' '.join(tokenized_texts[cnt:])

def extract_right_context(text, cnt):
    tokenized_texts = wordpunct_tokenize(text)
    return ' '.join(tokenized_texts[:cnt])

if __name__ == '__main__':
    context = 5
    with codecs.open('cadec_with_sct_codes.json') as in_file:
        reviews = map(json.loads, in_file.readlines())

    with codecs.open('dataset_{}.csv'.format(context), 'w') as out_file:
        for review in reviews:
            text = review['text']
            for entity_id, entity in review['entities'].iteritems():
                entity_start = entity['start']
                entity_end = entity['end']
                left_context = extract_left_context(text[:entity_start], context)
                right_context = extract_right_context(text[entity_end:], context)
                utterance = ' '.join([left_context, entity['text'], right_context]).lower()
                utterance = re.sub('^ ', '', utterance)
                utterance = re.sub(' $', '', utterance)
                if len(entity['sct_codes']) == 1:
                    out_file.write(utterance + '\t' + entity['sct_codes'][0] + '\n')
                """
                for code in entity['sct_codes']:
                    out_file.write(utterance + '\t' + code + '\n')
                if len(entity['sct_codes']) == 0:
                    out_file.write(utterance + '\t' + 'CONCEPT_LESS' + '\n')
                """

