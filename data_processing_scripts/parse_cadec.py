import codecs
import os
import re
import json



TEXTS_FOLDER = '../data/cadec/text/'
ORIGANAL_ANNOTATION_FOLDER = '../data/cadec/original/'
SCT_ANNOTATION_FOLDER = '../data/cadec/sct/'
MEDDRA_ANNOTATION_FOLDER = '../data/cadec/meddra/'


def extract_entities(original_annotation_path, text):
    with codecs.open(original_annotation_path) as in_file:
        annotations = [line.strip('\n').split('\t') for line in in_file if line.startswith('T')]
    entities = {}
    for annotation in annotations:
        entity_id = annotation[0]
        entity_text = annotation[2]
        positions = re.findall('\d+', annotation[1])
        positions = map(int, positions)
        entity_start = min(positions)
        entity_end = max(positions)
        entity_type = re.findall('\w+', annotation[1])[0]
        entities[entity_id] = {
                                'text':entity_text,
                                'start': entity_start,
                                'end': entity_end,
                                'type': entity_type
                            }

    return entities

def extract_entity_id(annotation): return re.findall('T+\d{,2}', annotation)[0]
def extract_codes(annotation): return re.findall('\d{7,}', annotation)

def add_codes(sct_annotation_file, meddra_annotation_file, entities):
    with codecs.open(sct_annotation_file) as in_file:
        sct_annotations = [line.strip('\n') for line in in_file]

    with codecs.open(meddra_annotation_file) as in_file:
        meddra_annotations = [line.strip('\n') for line in in_file]

    for sct_annotation in sct_annotations:
        entity_id = extract_entity_id(sct_annotation)
        sct_codes = extract_codes(sct_annotation)
        entities[entity_id[1:]]['sct_codes'] = sct_codes

    for meddra_annotation in meddra_annotations:
        entity_id = extract_entity_id(meddra_annotation)
        meddra_codes = extract_codes(meddra_annotation)
        entities[entity_id[1:]]['meddra_codes'] = meddra_codes

if __name__ == '__main__':
    text_files = sorted(os.listdir(TEXTS_FOLDER))
    original_annotation_files = sorted(os.listdir(ORIGANAL_ANNOTATION_FOLDER))
    sct_annotation_files = sorted(os.listdir(SCT_ANNOTATION_FOLDER))
    meddra_annotation_files = sorted(os.listdir(MEDDRA_ANNOTATION_FOLDER))
    reviews = []
    for text_file, original_annotation_file, sct_annotation_file, meddra_annotation_file in zip(text_files, original_annotation_files, sct_annotation_files, meddra_annotation_files):
            review = {}
            text_path = os.path.join(TEXTS_FOLDER, text_file)
            original_annotation_path = os.path.join(ORIGANAL_ANNOTATION_FOLDER, original_annotation_file)
            sct_annotation_path = os.path.join(SCT_ANNOTATION_FOLDER, sct_annotation_file)
            meddra_annotation_path = os.path.join(MEDDRA_ANNOTATION_FOLDER, meddra_annotation_file)

            with codecs.open(text_path) as in_file:
               text = in_file.read()

            entities = extract_entities(original_annotation_path, text)
            add_codes(sct_annotation_path, meddra_annotation_path, entities)
            review['text'] = text
            review['entities'] = entities
            review['file'] = text_file
            reviews.append(review)

    with codecs.open('cadec_with_sct_and_meddra_codes.json', 'w') as out_file:
        dumped_jsons = map(json.dumps, reviews)
        out_file.write('\n'.join(dumped_jsons))


