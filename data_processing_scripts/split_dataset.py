from pandas import read_csv
from collections import defaultdict
from sklearn.model_selection import KFold
import numpy as np
import pickle
import codecs
import os
import csv



DATASET_PATH = '../data/dataset_{}.csv'
FOLDS_FOLDER = '../data/folds/context_{}'
FOLDS_COUNT = 5
USE_ADDITIONAL_CODES = False

if __name__ == '__main__':
    np.random.seed(42)

    ###
    # finding indexes
    ###
    df = read_csv(DATASET_PATH.format(0), names=['phrase', 'code'], sep='\t', quoting=csv.QUOTE_NONE)
    df['ind'] = range(df.shape[0])
    df = df.groupby(['phrase', 'code']).min().reset_index()
    #df = df[df.code!='CONCEPT_LESS']
    folds_indexes = [defaultdict(list) for i in range(FOLDS_COUNT)]
    kf = KFold(FOLDS_COUNT)
    for name, group in df.groupby('code'):
        splits = None
        if group.shape[0] >= FOLDS_COUNT: splits = kf.split(group)
        for fold_indexes in folds_indexes:
            if splits is not None:
                train, test = splits.next()
                train = group.iloc[train].ind
                test = group.iloc[test].ind
                fold_indexes['train'] = np.append(fold_indexes['train'], train)
                fold_indexes['test'] = np.append(fold_indexes['test'], test)
            elif USE_ADDITIONAL_CODES:
                fold_indexes['train'] = np.append(fold_indexes['train'], group.ind)
    for context_size in range(5):
        folds_folder = FOLDS_FOLDER.format(context_size)
        dataset_path = DATASET_PATH.format(context_size)
        df = read_csv(dataset_path, names=['phrase', 'code'], sep='\t', quoting=csv.QUOTE_NONE)
        df['ind'] = range(df.shape[0])
        for i, fold_indexes in enumerate(folds_indexes):
            train_indexes = fold_indexes['train'].astype(np.int32)
            test_indexes = fold_indexes['test'].astype(np.int32)
            train_file_path = os.path.join(folds_folder, 'train_{}.csv'.format(i))
            test_file_path = os.path.join(folds_folder, 'test_{}.csv'.format(i))
            df[df.ind.isin(train_indexes)].sample(frac=1)[['phrase', 'code']].to_csv(train_file_path, index=False, header=None, sep='\t')
            df[df.ind.isin(test_indexes)][['phrase', 'code']].to_csv(test_file_path, index=False, header=None, sep='\t')



