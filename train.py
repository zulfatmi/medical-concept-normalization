from knowledge import *
import codecs
import numpy as np
from models import *
from gensim.models import KeyedVectors
from utils import *
from argparse import ArgumentParser
from spell_checker import SpellChecker


def train(w2v_model, train_file_path, test_file_path, batch_size, epochs, learning_rate, hidden_units, flat_format, context_size):

    utterances_train, masks_train, concepts_train = read_flat_data(train_file_path)
    sp = SpellChecker(utterances_train)
    utterances_test, masks_test, concepts_test = read_flat_data(test_file_path, spell_checker=sp)

    raw_train_utterances = map(' '.join, utterances_train)
    raw_test_utterances = map(' '.join, utterances_test)
    train_similarities = get_distances_to_nearest_concepts(raw_train_utterances)
    test_similarities = get_distances_to_nearest_concepts(raw_test_utterances)

    token_index, tokens = create_token_index(utterances_train.tolist())#+ utterances_test.tolist())
    concept_index, concepts = create_concept_index(concepts_train.tolist() + concepts_test.tolist())

    #X_train_pos_tags = get_pos_tags(utterances_train)
    #X_test_pos_tags = get_pos_tags(utterances_test)
    #pos_tag_index, pos_tags = create_token_index(X_train_pos_tags + X_test_pos_tags)

    X_train = tokens2ids(utterances_train, token_index)
    #X_train_pos_tags = tokens2ids(X_train_pos_tags, pos_tag_index)
    X_lenghts_train = map(len, X_train)
    input_max_length = max(X_lenghts_train)
    pad_to_max_len = lambda x: pad(x, input_max_length)
    X_train = map(pad_to_max_len, X_train)
    #X_train_pos_tags = map(pad_to_max_len, X_train_pos_tags)
    X_train = np.array(X_train)
    #X_train_pos_tags = np.array(X_train_pos_tags)
    X_lenghts_train = np.array(X_lenghts_train)

    #masks_train = map(lambda x: pad(x, input_max_length, padding_value=1), masks_train)

    y_train = concepts2ids(concepts_train, concept_index)
    y_train = np.array(y_train)

    ###
    # test
    ###
    X_test = tokens2ids(utterances_test, token_index)
    #X_test_pos_tags = tokens2ids(X_test_pos_tags, pos_tag_index)
    X_lenghts_test = map(len, X_test)
    X_lenghts_test = [min(length, input_max_length) for length in X_lenghts_test]
    X_test = map(pad_to_max_len, X_test)
    #X_test_pos_tags = map(pad_to_max_len, X_test_pos_tags)
    #X_test_pos_tags = np.array(X_test_pos_tags)
    X_test = np.array(X_test)

    #masks_test = map(lambda x: pad(x, input_max_length, padding_value=1), masks_test)

    y_test = concepts2ids(concepts_test, concept_index)
    y_test = np.array(y_test)

    tembeddings = get_embeddings(token_index, w2v_model, embeddings_dim=300)
    #pembeddings = get_embeddings(pos_tag_index, {}, embeddings_dim=3)
    vocabulary_size = len(token_index)
    #embeddings = np.random.randn(vocabulary_size, 400).astype(np.float32)
    classes_cnt = len(concept_index)

    model = AttnRNNNormalizer(input_max_len=input_max_length, hidden_units=hidden_units, tembeddings=tembeddings, classes_cnt=classes_cnt, learning_rate=learning_rate,
        train_embeddings=True)


    model.train(X_train, y_train, X_lenghts_train, train_similarities, batch_size=batch_size, epochs=epochs, validation_data=(X_test, y_test, X_lenghts_test, test_similarities))
    return model.accuracies







if __name__ == '__main__':

    parser = ArgumentParser(description='Normalizing NN training tool')
    parser.add_argument('--word2vec', '-w', dest='w2v_model_path', help='path to the word2vec model binary file')
    parser.add_argument('--trainfile', '-trf', dest='train_file_path', help='path to the training data file')
    parser.add_argument('--epochs', dest='epochs', type=int, default=30)
    parser.add_argument('--batch_size', dest='batch_size', type=int, default=32)
    parser.add_argument('--learning_rate', dest='learning_rate', type=float, default=1e-3)
    args = parser.parse_args()
    w2v_model = KeyedVectors.load_word2vec_format(args.w2v_model_path, binary=True)
    train(w2v_model, args.train_file_path, None, args.batch_size, args.epochs, args.learning_rate, flat_format=True)
