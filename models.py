import tensorflow as tf
import progressbar
import numpy as np
from datetime import datetime
from utils import batch_indexes

def weights_variable(shape):
    initial = tf.truncated_normal(shape, stddev=.005)
    return tf.Variable(initial)

def batches(X_train, y_train, X_lengths, batch_size):
    total_size = X_train.shape[0]
    total_steps = int(np.ceil(1.0 * total_size/batch_size))

    for i in range(total_steps):
        batch_start_pos = i*batch_size
        batch_end_pos = min(batch_start_pos + batch_size, total_size)
        yield batch_end_pos, X_train[batch_start_pos:batch_end_pos], y_train[batch_start_pos:batch_end_pos], X_lengths[batch_start_pos:batch_end_pos]

def extract_axis_1(data, ind):
    """
    Get specified elements along the first axis of tensor.
    :param data: Tensorflow tensor that will be subsetted.
    :param ind: Indices to take (one for each element along axis 0 of data).
    :return: Subsetted tensor.
    """

    batch_range = tf.range(tf.shape(data)[0])
    indices = tf.stack([batch_range, ind], axis=1)
    res = tf.gather_nd(data, indices)

    return res

def attention(inputs, attention_size, time_major=False, return_alphas=False):
    """
    Attention mechanism layer which reduces RNN/Bi-RNN outputs with Attention vector.
    The idea was proposed in the article by Z. Yang et al., "Hierarchical Attention Networks
     for Document Classification", 2016: http://www.aclweb.org/anthology/N16-1174.
    Args:
        inputs: The Attention inputs.
            Matches outputs of RNN/Bi-RNN layer (not final state):
                In case of RNN, this must be RNN outputs `Tensor`:
                    If time_major == False (default), this must be a tensor of shape:
                        `[batch_size, max_time, cell.output_size]`.
                    If time_major == True, this must be a tensor of shape:
                        `[max_time, batch_size, cell.output_size]`.
                In case of Bidirectional RNN, this must be a tuple (outputs_fw, outputs_bw) containing the forward and
                the backward RNN outputs `Tensor`.
                    If time_major == False (default),
                        outputs_fw is a `Tensor` shaped:
                        `[batch_size, max_time, cell_fw.output_size]`
                        and outputs_bw is a `Tensor` shaped:
                        `[batch_size, max_time, cell_bw.output_size]`.
                    If time_major == True,
                        outputs_fw is a `Tensor` shaped:
                        `[max_time, batch_size, cell_fw.output_size]`
                        and outputs_bw is a `Tensor` shaped:
                        `[max_time, batch_size, cell_bw.output_size]`.
        attention_size: Linear size of the Attention weights.
        time_major: The shape format of the `inputs` Tensors.
            If true, these `Tensors` must be shaped `[max_time, batch_size, depth]`.
            If false, these `Tensors` must be shaped `[batch_size, max_time, depth]`.
            Using `time_major = True` is a bit more efficient because it avoids
            transposes at the beginning and end of the RNN calculation.  However,
            most TensorFlow data is batch-major, so by default this function
            accepts input and emits output in batch-major form.
        return_alphas: Whether to return attention coefficients variable along with layer's output.
            Used for visualization purpose.
    Returns:
        The Attention output `Tensor`.
        In case of RNN, this will be a `Tensor` shaped:
            `[batch_size, cell.output_size]`.
        In case of Bidirectional RNN, this will be a `Tensor` shaped:
            `[batch_size, cell_fw.output_size + cell_bw.output_size]`.
    """

    if isinstance(inputs, tuple):
        # In case of Bi-RNN, concatenate the forward and the backward RNN outputs.
        inputs = tf.concat(inputs, 2)

    if time_major:
        # (T,B,D) => (B,T,D)
        inputs = tf.array_ops.transpose(inputs, [1, 0, 2])

    inputs_shape = inputs.shape
    sequence_length = inputs_shape[1].value  # the length of sequences processed in the antecedent RNN layer
    hidden_size = inputs_shape[2].value  # hidden size of the RNN layer

    # Attention mechanism
    W_omega = tf.Variable(tf.random_normal([hidden_size, attention_size], stddev=0.1))
    b_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))
    u_omega = tf.Variable(tf.random_normal([attention_size], stddev=0.1))

    v = tf.tanh(tf.matmul(tf.reshape(inputs, [-1, hidden_size]), W_omega) + tf.reshape(b_omega, [1, -1]))
    vu = tf.matmul(v, tf.reshape(u_omega, [-1, 1]))
    exps = tf.reshape(tf.exp(vu), [-1, sequence_length])
    alphas = exps / tf.reshape(tf.reduce_sum(exps, 1), [-1, 1])

    # Output of Bi-RNN is reduced with attention vector
    output = tf.reduce_sum(inputs * tf.reshape(alphas, [-1, sequence_length, 1]), 1)

    if not return_alphas:
        return output
    else:
        return output, alphas


class RNNNormalizer(object):
    def __init__(self, input_max_len, hidden_units, embeddings, classes_cnt, learning_rate=1e-3, train_embeddings=True):
        super(RNNNormalizer, self).__init__()
        self.embeddings = tf.Variable(embeddings, trainable=train_embeddings)
        self.classes_cnt = classes_cnt
        self.learning_rate = learning_rate
        self.accuracies = []

        self.input_placeholder = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        self.similarities_placeholder = tf.placeholder(dtype=tf.float32, shape=[None, 5000])
        self.output_placeholder = tf.placeholder(dtype=tf.int32, shape=[None])
        self.sequence_lengths = tf.placeholder(dtype=tf.int32, shape=[None])
        self.prob = tf.placeholder_with_default(1.0, shape=())

        self.hidden_units = hidden_units
        #self.lstm_fw = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        self.lstm_fw = tf.contrib.rnn.GRUCell(self.hidden_units)
        #self.lstm_fw = tf.nn.rnn_cell.DropoutWrapper(self.lstm_fw, output_keep_prob=0.5)
        #self.lstm_bw = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        self.lstm_bw = tf.contrib.rnn.GRUCell(self.hidden_units)
        #self.lstm_bw = tf.nn.rnn_cell.DropoutWrapper(self.lstm_bw, output_keep_prob=0.5)

        self.W_out = weights_variable([hidden_units*2, classes_cnt])
        self.b_out = weights_variable([classes_cnt])
        self.logits = self.__inference()
        self.loss = self.__loss()
        self.optimize = self.__optimize()
        self.predictions = self.__predict()
        config = tf.ConfigProto(allow_soft_placement=True)
        self.sess = tf.Session(config=config)
        self.saver = tf.train.Saver()
        self.sess.run(tf.global_variables_initializer())

    def __inference(self):
        with tf.name_scope('embeddings'):
            embedded = tf.nn.embedding_lookup(self.embeddings, self.input_placeholder)

        with tf.name_scope('lstm'):
            rnn_outputs, states = tf.nn.bidirectional_dynamic_rnn(self.lstm_fw, self.lstm_bw, embedded, sequence_length=self.sequence_lengths, dtype=tf.float32)

        rnn_outputs = tf.concat(rnn_outputs, axis=2)
        last_state = extract_axis_1(rnn_outputs, self.sequence_lengths-1)
        #similarities = tf.nn.dropout(self.similarities_placeholder, keep_prob=self.prob)
        #last_state = tf.concat([last_state, similarities], axis=-1)
        last_state = tf.nn.dropout(last_state, keep_prob=self.prob)
        return tf.matmul(last_state, self.W_out) + self.b_out


    def __loss(self):
        loss_on_batch = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.output_placeholder, logits=self.logits, name='xentropy')
        return tf.reduce_mean(loss_on_batch)

    def __optimize(self): return tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def __predict(self):
        predicted = tf.argmax(self.logits, axis=-1)
        return predicted

    def train(self, X_train, y_train, X_lengths, similarities_train, batch_size=32, epochs=10, validation_data=(None, None, None, None)):
        X_val, y_val, X_lengths_val, similarities_val = validation_data
        pbar = progressbar.ProgressBar(maxval=X_train.shape[0], widgets=[
                progressbar.DynamicMessage('Epoch'), # Static text
                ', ',
                progressbar.DynamicMessage('progress'),
                '%, ',
                progressbar.AdaptiveETA(),
                ', ',
                progressbar.DynamicMessage('loss'),
                ', ',
                progressbar.DynamicMessage('batch_loss'),
                ', ',
                progressbar.DynamicMessage('accuracy')
            ])
        dataset_size = X_train.shape[0]

        for epoch in range(epochs):
            total_loss = 0.0
            count = 0
            pbar.start()
            processed = 0
            for b_indexes in batch_indexes(batch_size, dataset_size):
                X_train_batch = X_train[b_indexes]
                y_train_batch = y_train[b_indexes]
                X_lengths_batch = X_lengths[b_indexes]
                simil_batch = similarities_train[b_indexes]
                feed_dict = {
                                self.input_placeholder: X_train_batch,
                                self.output_placeholder: y_train_batch,
                                self.sequence_lengths: X_lengths_batch,
                                self.similarities_placeholder: simil_batch,
                                self.prob: 0.5
                            }
                _, loss_value = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)
                total_loss += loss_value*batch_size
                processed = min(dataset_size, processed + batch_size)
                progress = 100.0 * processed / dataset_size
                kwargs = {
                            'progress':progress,
                            'loss':total_loss/processed,
                            'Epoch':epoch + 1,
                            'batch_loss': loss_value,
                            'accuracy': None
                        }
                pbar.update(processed, **kwargs)
            if X_val is not None and y_val is not None:
                predictions = self.predict(X_val, X_lengths_val, similarities_val)
                correct_predictions = np.equal(predictions, y_val)
                accuracy = 100.0*sum(correct_predictions)/correct_predictions.shape[0]
                self.accuracies.append(accuracy)
                kwargs.update({'accuracy': accuracy})
                pbar.update(processed, **kwargs)
            pbar.finish()
        self.accuracies = map(str, self.accuracies)
        self.accuracies = '\t'.join(self.accuracies)

    def predict(self, X, X_lengths, similarities):
        feed_dict = {
                        self.input_placeholder: X,
                        self.sequence_lengths: X_lengths,
                        self.similarities_placeholder: similarities
                    }
        return self.sess.run(self.predictions, feed_dict=feed_dict)

    def save(self, path=None):
        path = "saved_models/model_{}.ckpt".format(datetime.today())
        self.saver.save(self.sess, path)

    def restore_from_file(self, path):
        self.saver.restore(self.sess, path)



class CNNNormalizer(object):
    def __init__(self, input_max_len, filters_cnt, embeddings, classes_cnt, learning_rate=1e-3, window_sizes=[3, 5, 7], train_embeddings=True):
        super(CNNNormalizer, self).__init__()
        self.embeddings = tf.Variable(embeddings, trainable=train_embeddings)
        self.embeddings_dim = embeddings.shape[1]
        self.classes_cnt = classes_cnt
        self.learning_rate = learning_rate
        self.filters_cnt = filters_cnt
        self.convolutions_cnt = len(window_sizes)
        self.window_sizes = window_sizes
        self.input_max_len = input_max_len
        self.accuracies = []

        self.input_placeholder = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        self.output_placeholder = tf.placeholder(dtype=tf.int32, shape=[None])
        self.sequence_lengths = tf.placeholder(dtype=tf.int32, shape=[None])

        self.conv_weights = {}
        self.conv_biases = {}
        for window_size in window_sizes:
            self.conv_weights[window_size] = weights_variable([window_size, self.embeddings_dim, filters_cnt])
            self.conv_biases[window_size] = weights_variable([filters_cnt])
        self.resulting_size = self.input_max_len
	if self.resulting_size % 2 == 1: self.resulting_size += 1
	self.resulting_size /= 2
        self.resulting_size *= self.filters_cnt

        self.W_out = weights_variable([self.convolutions_cnt*self.resulting_size, classes_cnt])
        self.b_out = weights_variable([classes_cnt])
        #self.W_out = weights_variable([100,classes_cnt])
        #self.b_out = weights_variable([classes_cnt])
        self.logits = self.__inference()
        self.loss = self.__loss()
        self.optimize = self.__optimize()
        self.predict = self.__predict()
        config = tf.ConfigProto(allow_soft_placement=True)
        self.sess = tf.Session(config=config)
        self.sess.run(tf.global_variables_initializer())

    def __inference(self):
        with tf.name_scope('embeddings'):
            embedded = tf.nn.embedding_lookup(self.embeddings, self.input_placeholder)

        conv_outs = []
        for window_size in self.window_sizes:
            with tf.name_scope('conv_{}'.format(window_size)):
                conv_weights = self.conv_weights[window_size]
                conv_biases = self.conv_biases[window_size]
                h_conv = tf.nn.relu(tf.nn.conv1d(embedded, conv_weights, stride=1, padding='SAME') + conv_biases)
                h_max_pool = tf.layers.max_pooling1d(h_conv, pool_size=2, strides=2, padding='SAME')
                h_flat = tf.reshape(h_max_pool, [-1, self.resulting_size])
                conv_outs.append(h_flat)

        conv_out = tf.concat(conv_outs, 1)
        #conv_out = tf.nn.relu(tf.matmul(conv_out, self.W_relu) + self.b_relu)
        conv_out = tf.nn.dropout(conv_out, keep_prob=0.5)
        return tf.matmul(conv_out, self.W_out) + self.b_out


    def __loss(self):
        loss_on_batch = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.output_placeholder, logits=self.logits, name='xentropy')
        return tf.reduce_mean(loss_on_batch)

    def __optimize(self): return tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def __predict(self):
        predicted = tf.argmax(self.logits, axis=-1)
        return predicted

    def train(self, X_train, y_train, X_lengths, batch_size=32, epochs=10, validation_data=(None, None, None)):
        X_val, y_val, X_lengths_val = validation_data
        pbar = progressbar.ProgressBar(maxval=X_train.shape[0], widgets=[
                progressbar.DynamicMessage('Epoch'), # Static text
                ', ',
                progressbar.DynamicMessage('progress'),
                '%, ',
                progressbar.AdaptiveETA(),
                ', ',
                progressbar.DynamicMessage('loss'),
                ', ',
                progressbar.DynamicMessage('batch_loss'),
                ', ',
                progressbar.DynamicMessage('accuracy')
            ])

        dataset = tf.contrib.data.Dataset.from_tensor_slices((X_train, y_train, X_lengths))
        dataset_size = X_train.shape[0]
        batched_dataset = dataset.batch(batch_size)
        steps_count = int(np.ceil(1.0 * dataset_size/batch_size))

        for epoch in range(epochs):
            total_loss = 0.0
            count = 0
            pbar.start()
            iterator = batched_dataset.make_one_shot_iterator()
            next_element = iterator.get_next()
            for i in range(1, steps_count+1):
                X_train_batch, y_train_batch, X_lengths_batch = self.sess.run(next_element)
                feed_dict = {
                                self.input_placeholder: X_train_batch,
                                self.output_placeholder: y_train_batch,
                                self.sequence_lengths: X_lengths_batch
                            }
                _, loss_value = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)
                total_loss += loss_value*batch_size
                processed = min(dataset_size, i*batch_size)
                progress = 100.0 * processed / X_train.shape[0]
                kwargs = {
                            'progress':progress,
                            'loss':total_loss/processed,
                            'Epoch':epoch + 1,
                            'batch_loss': loss_value,
                            'accuracy': None
                        }
                pbar.update(processed, **kwargs)
            if X_val is not None and y_val is not None:
                feed_dict = {
                                self.input_placeholder: X_val,
                                self.output_placeholder: y_val,
                                self.sequence_lengths: X_lengths_val
                            }
                predictions = self.sess.run(self.predict, feed_dict=feed_dict)
                correct_predictions = np.equal(predictions, y_val)
                accuracy = 100.0*sum(correct_predictions)/correct_predictions.shape[0]
                self.accuracies.append(accuracy)
                kwargs.update({'accuracy': accuracy})
                pbar.update(processed, **kwargs)
            pbar.finish()
        self.accuracies = map(str, self.accuracies)
        self.accuracies = '\t'.join(self.accuracies)



class PFRNNNormalizer(object):
    def __init__(self, input_max_len, hidden_units, embeddings, classes_cnt, context_size, learning_rate=1e-3, train_embeddings=True):
        super(PFRNNNormalizer, self).__init__()
        self.embeddings = tf.Variable(embeddings, trainable=train_embeddings)
        self.classes_cnt = classes_cnt
        self.learning_rate = learning_rate
        self.accuracies = []
        self.input_max_len = input_max_len
        self.context_size = context_size

        self.input_placeholder = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        self.mask_placeholder = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        self.output_placeholder = tf.placeholder(dtype=tf.int32, shape=[None])
        self.sequence_lengths = tf.placeholder(dtype=tf.int32, shape=[None])

        self.hidden_units = hidden_units
        self.lstm_fw = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        self.lstm_fw = tf.nn.rnn_cell.DropoutWrapper(self.lstm_fw, output_keep_prob=0.5)
        self.lstm_bw = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        self.lstm_bw = tf.nn.rnn_cell.DropoutWrapper(self.lstm_bw, output_keep_prob=0.5)

        self.W_out = weights_variable([hidden_units*2, classes_cnt])
        self.b_out = weights_variable([classes_cnt])


        self.logits = self.__inference()
        self.loss = self.__loss()
        self.optimize = self.__optimize()
        self.predictions = self.__predict()
        config = tf.ConfigProto(allow_soft_placement=True)
        self.sess = tf.Session(config=config)
        self.saver = tf.train.Saver()
        self.sess.run(tf.global_variables_initializer())

    def __inference(self):
        with tf.name_scope('embeddings'):
            embedded = tf.nn.embedding_lookup(self.embeddings, self.input_placeholder)

        with tf.name_scope('embeddings'):
            mask_embedded = tf.one_hot(self.mask_placeholder, self.context_size + 1)
        embedded = tf.concat([embedded, mask_embedded], axis=-1)

        with tf.name_scope('lstm'):
            rnn_outputs, states = tf.nn.bidirectional_dynamic_rnn(self.lstm_fw, self.lstm_bw, embedded, sequence_length=self.sequence_lengths, dtype=tf.float32)

        rnn_outputs = tf.concat(rnn_outputs, axis=2)
        #print self.attention.shape

        last_state = extract_axis_1(rnn_outputs, self.sequence_lengths-1)
        last_state = tf.nn.dropout(last_state, keep_prob=0.5)
        return tf.matmul(last_state, self.W_out) + self.b_out



    def __loss(self):
        loss_on_batch = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.output_placeholder, logits=self.logits, name='xentropy')
        return tf.reduce_mean(loss_on_batch)

    def __optimize(self): return tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def __predict(self):
        predicted = tf.argmax(self.logits, axis=-1)
        return predicted

    def train(self, X_train, y_train, X_lengths, masks_train, batch_size=32, epochs=10, validation_data=(None, None, None, None)):
        X_val, y_val, X_lengths_val, masks_val = validation_data
        pbar = progressbar.ProgressBar(maxval=X_train.shape[0], widgets=[
                progressbar.DynamicMessage('Epoch'), # Static text
                ', ',
                progressbar.DynamicMessage('progress'),
                '%, ',
                progressbar.AdaptiveETA(),
                ', ',
                progressbar.DynamicMessage('loss'),
                ', ',
                progressbar.DynamicMessage('batch_loss'),
                ', ',
                progressbar.DynamicMessage('accuracy')
            ])
        dataset = tf.contrib.data.Dataset.from_tensor_slices((X_train, y_train, X_lengths, masks_train))
        dataset_size = X_train.shape[0]
        batched_dataset = dataset.batch(batch_size)
        steps_count = int(np.ceil(1.0 * dataset_size/batch_size))

        for epoch in range(epochs):
            total_loss = 0.0
            count = 0
            pbar.start()
            iterator = batched_dataset.make_one_shot_iterator()
            next_element = iterator.get_next()
            for i in range(1, steps_count+1):
                X_train_batch, y_train_batch, X_lengths_batch, masks_batch = self.sess.run(next_element)
                feed_dict = {
                                self.input_placeholder: X_train_batch,
                                self.mask_placeholder: masks_batch,
                                self.output_placeholder: y_train_batch,
                                self.sequence_lengths: X_lengths_batch
                            }
                _, loss_value = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)
                total_loss += loss_value*batch_size
                processed = min(dataset_size, i*batch_size)
                progress = 100.0 * processed / X_train.shape[0]
                kwargs = {
                            'progress':progress,
                            'loss':total_loss/processed,
                            'Epoch':epoch + 1,
                            'batch_loss': loss_value,
                            'accuracy': None
                        }
                pbar.update(processed, **kwargs)
            if X_val is not None and y_val is not None:
                predictions = self.predict(X_val, X_lengths_val, masks_val)
                correct_predictions = np.equal(predictions, y_val)
                accuracy = 100.0*sum(correct_predictions)/correct_predictions.shape[0]
                self.accuracies.append(accuracy)
                kwargs.update({'accuracy': accuracy})
                pbar.update(processed, **kwargs)
            pbar.finish()
        self.accuracies = map(str, self.accuracies)
        self.accuracies = '\t'.join(self.accuracies)

    def predict(self, X, X_lengths, masks):
        feed_dict = {
                        self.input_placeholder: X,
                        self.mask_placeholder: masks,
                        self.sequence_lengths: X_lengths
                    }
        return self.sess.run(self.predictions, feed_dict=feed_dict)

    def save(self, path=None):
        path = "saved_models/model_{}.ckpt".format(datetime.today())
        self.saver.save(self.sess, path)

    def restore_from_file(self, path):
        self.saver.restore(self.sess, path)


class AttnRNNNormalizer(object):

    def __init__(self, input_max_len, hidden_units, tembeddings,
                 classes_cnt, learning_rate=1e-3,
                 attention_size=2, train_embeddings=True):

        super(AttnRNNNormalizer, self).__init__()
        self.tembeddings = tf.Variable(tembeddings, trainable=train_embeddings)
        #self.pembeddings = tf.Variable(pembeddings, trainable=True)
        self.classes_cnt = classes_cnt
        self.learning_rate = learning_rate
        self.attention_size = attention_size
        self.accuracies = []

        self.input_placeholder = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        #self.pos_input_placeholder = tf.placeholder(dtype=tf.int32, shape=[None, input_max_len])
        self.similarities_placeholder = tf.placeholder(dtype=tf.float32, shape=[None, 470])
        self.output_placeholder = tf.placeholder(dtype=tf.int32, shape=[None])
        self.sequence_lengths = tf.placeholder(dtype=tf.int32, shape=[None])
        self.prob = tf.placeholder_with_default(1.0, shape=())

        self.hidden_units = hidden_units
        #self.lstm_fw = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        self.lstm_fw = tf.contrib.rnn.GRUCell(self.hidden_units)
        #self.lstm_fw = tf.nn.rnn_cell.DropoutWrapper(self.lstm_fw, output_keep_prob=0.5)
        self.lstm_bw = tf.contrib.rnn.GRUCell(self.hidden_units)
        #self.lstm_bw = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        #self.lstm_bw_2 = tf.contrib.rnn.BasicLSTMCell(self.hidden_units)
        #self.lstm_bw = tf.nn.rnn_cell.DropoutWrapper(self.lstm_bw, output_keep_prob=0.5)

        self.W = weights_variable([hidden_units*2, classes_cnt])
        self.b = weights_variable([classes_cnt])
        self.logits = self.__inference()
        self.loss = self.__loss()
        self.optimize = self.__optimize()
        self.predictions = self.__predict()
        config = tf.ConfigProto(allow_soft_placement=True)
        self.sess = tf.Session(config=config)
        self.saver = tf.train.Saver()
        self.sess.run(tf.global_variables_initializer())

    def __inference(self):

        #with tf.name_scope('pos_tag_embeddings'):
        #    pembedded = tf.nn.embedding_lookup(self.pembeddings, self.pos_input_placeholder)

        with tf.name_scope('token_embeddings'):
            tembedded = tf.nn.embedding_lookup(self.tembeddings, self.input_placeholder)

        embedded = tembedded #tf.concat([pembedded, tembedded], axis=-1)
        with tf.variable_scope('lstm'):
            rnn_outputs, states = tf.nn.bidirectional_dynamic_rnn(self.lstm_fw, self.lstm_bw, embedded, sequence_length=self.sequence_lengths, dtype=tf.float32)

        #rnn_outputs = tf.concat(rnn_outputs, axis=2)
        #with tf.variable_scope('lstm2'):
        #    rnn_outputs, states = tf.nn.bidirectional_dynamic_rnn(self.lstm_fw_2, self.lstm_bw_2, rnn_outputs, sequence_length=self.sequence_lengths, dtype=tf.float32)


        attn_out = attention(rnn_outputs, self.attention_size, return_alphas=False)
        #similarities = tf.nn.dropout(self.similarities_placeholder, keep_prob=self.prob)
        #attn_out = tf.concat([attn_out, similarities], axis=-1)
        attn_out = tf.nn.dropout(attn_out, keep_prob=self.prob)
        return tf.matmul(attn_out, self.W) + self.b


    def __loss(self):
        loss_on_batch = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=self.output_placeholder, logits=self.logits, name='xentropy')
        return tf.reduce_mean(loss_on_batch)

    def __optimize(self): return tf.train.AdamOptimizer(self.learning_rate).minimize(self.loss)

    def __predict(self):
        predicted = tf.argmax(self.logits, axis=-1)
        return predicted

    def train(self, X_train, y_train, X_lengths, similarities_train, batch_size=32, epochs=10, validation_data=(None, None, None, None)):
        X_val, y_val, X_lengths_val, similarities_val = validation_data
        pbar = progressbar.ProgressBar(maxval=X_train.shape[0], widgets=[
                progressbar.DynamicMessage('Epoch'), # Static text
                ', ',
                progressbar.DynamicMessage('progress'),
                '%, ',
                progressbar.AdaptiveETA(),
                ', ',
                progressbar.DynamicMessage('loss'),
                ', ',
                progressbar.DynamicMessage('batch_loss'),
                ', ',
                progressbar.DynamicMessage('accuracy')
            ])

        dataset_size = X_train.shape[0]

        for epoch in range(epochs):
            total_loss = 0.0
            count = 0
            pbar.start()
            processed = 0
            for b_indexes in batch_indexes(batch_size, dataset_size):
                X_train_batch = X_train[b_indexes]
                y_train_batch = y_train[b_indexes]
                X_lengths_batch = X_lengths[b_indexes]
                simil_batch = similarities_train[b_indexes]
                feed_dict = {
                                self.input_placeholder: X_train_batch,
                                self.output_placeholder: y_train_batch,
                                self.sequence_lengths: X_lengths_batch,
                                self.similarities_placeholder: simil_batch,
                                self.prob: 0.5
                            }
                _, loss_value = self.sess.run([self.optimize, self.loss], feed_dict=feed_dict)
                total_loss += loss_value*batch_size
                processed = min(dataset_size, processed + batch_size)
                progress = 100.0 * processed / dataset_size
                kwargs = {
                            'progress':progress,
                            'loss':total_loss/processed,
                            'Epoch':epoch + 1,
                            'batch_loss': loss_value,
                            'accuracy': None
                        }
                pbar.update(processed, **kwargs)
            if X_val is not None and y_val is not None:
                predictions = self.predict(X_val, X_lengths_val, similarities_val)
                correct_predictions = np.equal(predictions, y_val)
                accuracy = 100.0*sum(correct_predictions)/correct_predictions.shape[0]
                self.accuracies.append(accuracy)
                kwargs.update({'accuracy': accuracy})
                pbar.update(processed, **kwargs)
            pbar.finish()
        self.accuracies = map(str, self.accuracies)
        self.accuracies = '\t'.join(self.accuracies)

    def predict(self, X, X_lengths, similarities_val):
        feed_dict = {
                        self.input_placeholder: X,
                        #self.output_placeholder: y_val,
                        self.sequence_lengths: X_lengths,
                        self.similarities_placeholder:similarities_val
                    }
        return self.sess.run(self.predictions, feed_dict=feed_dict)

    def save(self, path=None):
        path = "saved_models/model_{}.ckpt".format(datetime.today())
        self.saver.save(self.sess, path)

    def restore_from_file(self, path):
        self.saver.restore(self.sess, path)
